How to run
==========

python ../main/run_ner_write_probs.py --data_dir=data --model_name_or_path=bert-base-cased --output_dir=data/output

python ../main/template.py  --data_dir=data --config_file=config.json --output_file=preds


