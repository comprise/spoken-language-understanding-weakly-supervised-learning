import torch
from transformers import BertTokenizer, BertForMaskedLM
import logging
import pandas as pd
from datasets import Dataset
from collections import Counter, defaultdict
from tqdm import tqdm
import numpy as np
from scipy.special import softmax
import spacy
import argparse
from seqeval.metrics import f1_score, classification_report
import json


def get_dataset(file):
  df = pd.read_csv(file, sep=" ", header=None, usecols=[0, 3],
                   skip_blank_lines=False)
  df.columns = ['token', 'tag']
  df['tag'] = df['tag'].fillna('O')
  df.token[df.token == ' '] = '"'
  dataset = Dataset.from_pandas(df)

  return dataset

def get_dataset_native_py(file):
  tokens = []
  tags = []
  with open(file) as f:
      for line in f:
        _list = line.split()
        if len(_list) == 4:
          tokens.append(_list[0])
          tags.append(_list[-1])
        elif len(_list) == 0:
          tokens.append(None)
          tags.append('O')
        else:
          raise print(_list)

  df = pd.DataFrame({'token': tokens, 'tag': tags })
  df['tag'] = df['tag'].fillna('O')
  df.token[df.token == ' '] = '"'
  dataset = Dataset.from_pandas(df)

  return dataset



def get_pred_dataset(file):
  df = pd.read_csv(file, sep=" ", header=None, skip_blank_lines=False,
                   quoting=3)
  df.rename(columns={0: 'token', 1: 'pred_tag'}, inplace=True)
  df['pred_probs'] = df.values[:, 2:].tolist()
  df['pred_probs'] = df['pred_probs'].apply(lambda x: softmax(x))
  df['pred_tag'] = df['pred_tag'].fillna('O')
  dataset = Dataset.from_pandas(df)

  return dataset


def get_word_ids(words):
  ids = []
  for w in words:
    _id = tokenizer.convert_tokens_to_ids(w)
    ids.append(_id)

  assert len(ids) == len(words)
  return ids


ent_pos = ['PROPN', 'ADV', 'ADJ']


def get_pos(seq):
  text = " ".join(seq)
  doc = nlp(text)
  token_tags = []
  for token in doc:
    if token.pos_ == 'SPACE':
      token_tags.append([' ', token.pos_])
    elif len(token) == 1:
      token_tags.append([token.text, 'O'])
    else:
      token_tags.append([token.text, token.pos_])

  seq_tags = []
  counter = 0
  running_token = ''
  running_tags = []
  for t, pos in token_tags:
    if t == seq[counter]:
      seq_tags.append([t, pos])
      counter += 1
    else:
      running_token += t
      running_tags += [pos]
      if running_token == seq[counter]:
        if ent_pos in running_tags:
          _pos = ent_pos
        else:
          _pos = running_tags[0]
        seq_tags.append([running_token, _pos])
        counter += 1
        running_token = ''
        running_tags = []

  assert seq == [t for t, _ in seq_tags]

  for i in range(len(seq)):
    if seq_tags[i][1] == ent_pos:
      seq_tags[i][0] = seq_tags[i][0].capitalize()
      seq[i] = seq[i].capitalize()
  seq[0] = seq[0].capitalize()
  return seq, seq_tags

def get_labels(text, template_text, ner_words_):
  #print(text)
  #print(template_text)
  tokenized_text = tokenizer.tokenize(text)
  tokenized_template = tokenizer.tokenize(template_text)
  _tokenized_text = tokenized_text + tokenized_template
  masked_ids = [i for i, x in enumerate(_tokenized_text) if x == "[MASK]"]
  indexed_tokens = tokenizer.convert_tokens_to_ids(_tokenized_text)
  tokens_tensor = torch.tensor([indexed_tokens]).to(device)

  segments_ids_text = [0] * len(tokenized_text)
  segments_ids_template = [1] * len(tokenized_template)
  segments_ids = segments_ids_text + segments_ids_template
  segments_tensors = torch.tensor([segments_ids]).to(device)

  outputs = model(tokens_tensor, token_type_ids=segments_tensors)
  predictions = outputs[0]
  prob = torch.softmax(predictions[0, masked_ids], dim=-1)

  labels_dic = {}
  for tag, ids in ner_words_.items():
      labels_dic[tag] = prob[0, ids].max().item()
  #print(labels_dic)
  pred_label = max(labels_dic, key=labels_dic.get)
  pred_prob = labels_dic[pred_label]
  return pred_label, pred_prob



def classifier(seq, pos_tags):
  text_seq = ["[CLS]"] + seq + ["[SEP]"]
  text =  ' '.join(text_seq)

  len_pos = len(pos_tags)
  pred_tags = []
  _entity = ''
  counter = 0
  for i, (t, pos) in enumerate(pos_tags):
    if pos not in ent_pos:
      pred_tags.append('O')
      _entity = ''
      counter = 0
      continue

    _entity += ' ' + t
    counter += 1
    if i+1 < len_pos and pos_tags[i+1][1] in ent_pos:
      continue
    else:
      pred_tag = 'O'
      pred_prob = threshold
      _ner_words = ner_words
      _templates = templates[:2]
      if pos in ent_pos[1:]:
        pred_prob = threshold
        _ner_words = adv_adj_words
        _templates = templates[2:]
      for template in _templates:
        template_text = _entity +  template
        _pred_label, _pred_prob = get_labels(text, template_text, _ner_words)
        if _pred_prob > pred_prob:
          pred_prob = _pred_prob
          pred_tag = _pred_label
      pred_tags += [pred_tag] * counter

  assert len(pred_tags) == len(seq), print(seq, len(pred_tags), len(seq))

  bio_pred_tags = []
  for i, tag in enumerate(pred_tags):
    if tag == 'O':
      bio_pred_tags += [tag]
      continue
    if len(bio_pred_tags) == 0:
      bio_pred_tags += ['B-'+tag]

    elif tag in bio_pred_tags[i-1]:
      bio_pred_tags += ['I-'+tag]
    else:
      bio_pred_tags += ['B-'+tag]

  return bio_pred_tags


def flatten(_list):
  return [item for sublist in _list for item in sublist]


def analyze_predictions(gt_dataset, pred_dataset):
  gt_dataset_tokens = gt_dataset['token']
  gt_dataset_tags = gt_dataset['tag']

  pred_dataset_tokens = pred_dataset['token']
  pred_dataset_pred_tags = pred_dataset['pred_tag']
  pred_dataset_probs = pred_dataset['pred_probs']

  seqs = []
  gt_tags = []
  pred_tags = []

  seq = []
  gt_tag = []
  pred_tag = []

  num_example = 0
  for i, token in enumerate(gt_dataset_tokens):
    assert token == pred_dataset_tokens[i], print(i, token, pred_dataset_tokens[i])
    _gt_tag = gt_dataset_tags[i]
    _pred_tag = pred_dataset_pred_tags[i]
    _prob = pred_dataset_probs[i]
    if token == None:
      if len(seq) > 0:
        seq, pos_tags = get_pos(seq)
        pred_tag_template = classifier(seq, pos_tags)
        final_seq_tag = []
        for k, label in enumerate(pred_tag_template):
          if label[2:] in ['PERSON', 'LOC', 'ORDINAL', 'LANGUAGE']:
            final_seq_tag.append(label)
          else:
            final_seq_tag.append(pred_tag[k])

        seqs.append(seq)
        gt_tags.append(gt_tag)
        pred_tags.append(final_seq_tag)

        seq = []
        gt_tag = []
        pred_tag = []
        wrong_pred = []
        num_example += 1

        if num_example % 200 == 0:
          print(classification_report(gt_tags, pred_tags))
      else:
        continue
    else:
      seq += [token]
      gt_tag += [_gt_tag]
      pred_tag += [_pred_tag]

  print(classification_report(gt_tags, pred_tags))
  return seqs, pred_tags






if __name__ == '__main__':
  parser = argparse.ArgumentParser(description='template')
  parser.add_argument('-d','--data_dir', help='data direcotry', required=True)
  parser.add_argument('-cf','--config_file', help='config file', required=True)
  parser.add_argument('-of','--output_file', help='output file', required=True)
  args = vars(parser.parse_args())

  test_file = args['data_dir'] + '/test.txt'
  test_pred_file = args['data_dir'] + '/test_predictions.txt'

  cf = open(args['config_file'], )
  config = json.load(cf)



  pred_test_dataset = get_pred_dataset(test_pred_file)
  test_dataset = get_dataset_native_py(test_file)

  templates = [" ".join(config['template'].split(" ")[1:])]

  nlp = spacy.load("en_core_web_sm")
  device = 'cpu'
  model_name = 'bert-large-cased'
  model = BertForMaskedLM.from_pretrained(model_name).to(device)
  tokenizer = BertTokenizer.from_pretrained(model_name)

  ner_words = {k: get_word_ids(v) for k, v in config["labels"].items()}
  adv_adj_words = {k: v for k, v in ner_words.items() if k in ['ORDINAL', 'LANGUAGE']}
  threshold = config['threshold']
  seqs, pred_tags = analyze_predictions(test_dataset[0: 32], pred_test_dataset[0: 32])
  with open(args['output_file'], 'w') as f:
    for i, tags in enumerate(pred_tags):
      for j, tag in enumerate(tags):
        tmp = seqs[i][j] + ' ' + tag
        f.write("%s\n" % tmp)



