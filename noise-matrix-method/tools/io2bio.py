# Copyright 2020 Saarland University, Spoken Language Systems LSV
# Authors: Lukas Lange, Michael A. Hedderich, Dietrich Klakow, Thomas Kleinbauer
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
# THIS CODE IS PROVIDED *AS IS*, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
# WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
# MERCHANTABLITY OR NON-INFRINGEMENT.
#
# See the Apache 2 License for the specific language governing permissions and
# limitations under the License.

import sys

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print("USAGE: python3 {} <infile.tsv> <outfile.tsv>".format(sys.argv[0]))
        sys.exit(1)

    prev_label = None
    with open(sys.argv[1], 'r') as infile:
        with open(sys.argv[2], 'w') as outfile:
            for line in infile:
                split = line.strip().split('\t')
                if len(split) == 2:
                    label = split[1]
                    if label == 'O':
                        outfile.write(line)
                    elif label == prev_label:
                        outfile.write("{}\tI-{}\n".format(split[0], label))
                    else:
                        outfile.write("{}\tB-{}\n".format(split[0], label))
                    prev_label = label
                else:
                    outfile.write(line)
