# Copyright 2020 Saarland University, Spoken Language Systems LSV
# Authors: Lukas Lange, Michael A. Hedderich, Dietrich Klakow, Thomas Kleinbauer
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
# THIS CODE IS PROVIDED *AS IS*, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
# WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
# MERCHANTABLITY OR NON-INFRINGEMENT.
#
# See the Apache 2 License for the specific language governing permissions and
# limitations under the License.

import sys
sys.path.append('../main')

from functools import reduce
from utils import create_pickle_data
from ner_datacode import LabelRepresentation, WordEmbedding
import os
import tempfile


LABELS = { 'verbmobil': ['O', 'PER', 'ORG', 'LOC', 'DATE', 'TIME'] }
          
PATH_TO_FASTTEXT = '/data/corpora/fasttext/cc.en.300.bin'

if __name__ == '__main__':
    if len(sys.argv) != 5:
        print("USAGE: python3 {} <label-set> <label-format> <input.tsv> <output.pickle>".format(sys.argv[0]))
        print("where:")
        if len(LABELS) == 1:
            print("\t<label-set>\tthe string '{}'".format(next(iter(LABELS.keys()))))
        else:
            print("\t<label-set>\tone of", ','.join(["'{}'".format(lf) for lf in LABELS.keys()]))
        print("\t<label-format>\teither 'bio' or 'io'")
        print("\t<input.tsv>\tan annotated file in tab-separated format")
        print("\t<output.pickle>\tthe name of the file to be created by this program")
        sys.exit(1)

    if not sys.argv[1] in list(LABELS.keys()) + ['conll']:
        print("Unknown label set '{}', must be one of:".format(sys.argv[1]))
        for label_set_name in LABELS:
            print(" -", label_set_name)
        sys.exit(1)

    if not sys.argv[2] in ['io', 'bio']:
        print("Unknown label format '{}', must be either 'io' or 'bio'".format(sys.argv[2]))
        sys.exit(1)


    #labels = sorted(LABELS[sys.argv[1]])
    label_format = sys.argv[2]
    in_file = sys.argv[3]
    out_file = sys.argv[4]
    save_root = os.path.dirname(out_file)
    save_name = os.path.basename(out_file)

    remove_label_prefix = label_format == "io"

    label_representation = LabelRepresentation()

    if sys.argv[1] == 'conll':
        if label_format == 'bio':
            label_representation.use_connl_bio_labels()
        else:
            label_representation.use_connl_io_labels()
    else:
        labels = LABELS[sys.argv[1]]
        
        if label_format == 'bio':
            bio_labels = []
            for label in labels:
                if label == 'O':
                    bio_labels.append(label)
                else:
                    bio_labels.append('B-' + label)
                    bio_labels.append('I-' + label)
            labels = bio_labels

        label_map = {label:i for i,label in enumerate(labels)}
        label_representation.use_specific_label_map(label_map)
    
    # extract labels from input file
    rewrite_data = False
    data = []

    with open(in_file, 'r') as f:
        lines = f.readlines()

        for line in lines:
            s = line.strip().split('\t')
            
            if len(s) == 2:
                label = s[1]

                # remove B- prefix if desired
                if remove_label_prefix:
                    if label[:2] == 'B-' or label[:2] == 'I-':
                        label = label[2:]
                        if not rewrite_data:
                            print("Cutting off labels to conform to 'io' format.")
                        rewrite_data = True

                # check label validity
                if label not in label_representation.label_name_to_label_idx_map:
                    print("Found unknown label '{}'. Aborting.".format(label))
                    sys.exit(2)
                    
                data.append('{}\t{}\n'.format(s[0], label))
            else:
                data.append(line)

    if rewrite_data:
        file_handle, path_to_data = tempfile.mkstemp()
        f = os.fdopen(file_handle, 'w')
        for line in data:
            f.write(line)
        f.close()
    else:
        path_to_data = in_file
        data = None
                
    input_separator = '\t'
    
    word_embedding = WordEmbedding()
    word_embedding.load_fasttext(PATH_TO_FASTTEXT)

    context_length = 3

    create_pickle_data(path_to_data, label_representation, remove_label_prefix,
                       input_separator, word_embedding, context_length,
                       save_root, save_name)

    # if we created a temporary file above, let's delete it again here
    if rewrite_data:
        os.remove(path_to_data)
