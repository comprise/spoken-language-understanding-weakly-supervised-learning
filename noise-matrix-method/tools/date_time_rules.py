# Copyright 2020 Saarland University, Spoken Language Systems LSV
# Authors: Lukas Lange, Michael A. Hedderich, Dietrich Klakow, Thomas Kleinbauer
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
# THIS CODE IS PROVIDED *AS IS*, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
# WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
# MERCHANTABLITY OR NON-INFRINGEMENT.
#
# See the Apache 2 License for the specific language governing permissions and
# limitations under the License.

import os
import re
import sys

from tqdm import tqdm

# DEBUG
import random
DEBUG_RULE_ENGINE=False

class Sentence:
    def __init__(self, words=None, labels=None):
        self.words = words if words else list()
        self.labels = labels if labels else list()

        while len(self.labels) < len(self.words):
            self.labels.append(None)

    def append(self, word, label):
        self.words.append(word)
        self.labels.append(label)

    def __repr__(self):
        return ' '.join(["{}/{}".format(w, l) if l else w for w,l in zip(self.words, self.labels)])

        
def read_data(input_file_name):
    sentences = [Sentence()]
    
    with open(input_file_name, 'r') as f:
        for line_number, line in enumerate(f):
            split = line.strip().split('\t')

            if len(split) == 0:
                if sentences[-1].words:
                    sentences.append(Sentence())
            elif len(split) == 1:
                sentences[-1].append(split[0], None)
            elif len(split) == 2:
                sentences[-1].append(*split)
            else:
                print("Cannot parse file '{}':".format(input_file_name))
                print("[{}]: {}".format(line_number, line))
                sys.exit(2)

    while not sentences[-1].words:
        sentences = sentences[:-1]

    return sentences


def all_matches(pattern, word_list, allow_overlap=False, pattern_to_lower=False):
    pattern = normalized_pattern(pattern, pattern_to_lower)
    
    if not word_list:
        return matches_beginning(pattern, [])
    else:
        ret = []

        for i in range(len(word_list)):
            result = matches_beginning(pattern, word_list[i:])
            if result is not None:
                if allow_overlap or not ret or i >= ret[-1][1]:
                    ret.append((i, i + result[0]))

        return ret if ret else None
    
    return None
    

def first_match(pattern, word_list, pattern_to_lower=False):
    # DEBUG
    if DEBUG_RULE_ENGINE:
        print("[matches] pattern: {}, word_list: {}".format(pattern, word_list))

    pattern = normalized_pattern(pattern, pattern_to_lower)
    
    if not word_list:
        return matches_beginning(pattern, [])
    else:
        for i in range(len(word_list)):
            result = matches_beginning(pattern, word_list[i:])
            if result is not None:
                return i, i + result[0]

    return None


def normalized_pattern(pattern, to_lower=False):
    if pattern is None:
        return None
    elif isinstance(pattern, bool):
        return pattern
    elif isinstance(pattern, tuple):
        return tuple([normalized_pattern(p, to_lower) for p in pattern])
    elif isinstance(pattern, list):
        return [normalized_pattern(p, to_lower) for p in pattern]
    elif isinstance(pattern, dict):
        if 'pattern' in pattern:
            if '*' in pattern or '+'in pattern or '?' in pattern:
                raise ValueError("Dict patterns must not contain 'pattern' and '*/+/?' keys.")
            pattern['pattern'] = normalized_pattern(pattern['pattern'], to_lower)
            return pattern
        elif '*' in pattern or '+' in pattern or '?' in pattern:
            key_found = False
            for key in ['*', '+', '?']:
                if key in pattern:
                    if key_found:
                        raise ValueError("Dict patterns must not contain multiple '*/+/?' keys.")

                    if 'min' in pattern or 'max' in pattern:
                        raise ValueError("Dict patterns must not contain '*/+/?' and 'min/max' keys.")
                    pattern['pattern'] = normalized_pattern(pattern[key], to_lower)
                    if key == '+':
                        pattern['min'] = 1
                    if key == '?':
                        pattern['max'] = 1
                    del pattern[key]

                    key_found = True

            return pattern
        else:
            raise ValueError("Dict patterns must contain a 'pattern' entry.")
    elif isinstance(pattern, str):
        if pattern == '*':
            return { 'pattern':True }
        elif pattern == '+':
            return { 'pattern':True, 'min':1 }
        else:
            return pattern.lower() if to_lower else pattern
    else:
        raise ValueError("Illegal pattern type '{}': {}".format(type(pattern), pattern))


def matches_beginning(pattern, word_list, prev_match=None):
    # DEBUG
    if DEBUG_RULE_ENGINE:
        print("[matches_beginning] pattern: {}, word_list: {}, prev_match: {}".format(pattern, word_list, prev_match))

    if isinstance(pattern, bool):
        if pattern and not prev_match:
            return 1, True
        else:
            return None                
    if isinstance(pattern, tuple):
        # alternatives
        start_index, sub_prev_match = (0, None) if prev_match is None else prev_match[1]
        
        for i in range(start_index, len(pattern)):
            sub_result = matches_beginning(pattern[i], word_list, sub_prev_match)
            if sub_result is None:
                sub_prev_match = None
            else:
                return sub_result[0], (i, sub_result)
                
        return None
    
    elif isinstance(pattern, list):
        # sequence
        if prev_match is None:
            sub_results = [None] * len(pattern)
            i = 0
        else:
            sub_results = prev_match[1]
            i = len(pattern) - 1
        
        while i >= 0 and i < len(pattern):
            word_list_offset = sum([s[0] for s in sub_results[:i]])
            sub_results[i] = matches_beginning(pattern[i], word_list[word_list_offset:], sub_results[i])

            # DEBUG
            if DEBUG_RULE_ENGINE and i >= 0:
                print("pattern[i]: {}, word_list_offset: {}, word_list[word_list_offset]: {} => sub_result: {}".format(pattern[i], word_list_offset, word_list[word_list_offset:], sub_results[i]))

            i += 1 if sub_results[i] is not None else -1

        if i < 0:
            return None
        else:
            return sub_results[-1][0] + word_list_offset, sub_results
        
    elif isinstance(pattern, dict):
        sub_pattern = pattern['pattern']
        at_least = pattern['min'] if 'min' in pattern else 0
        at_most = pattern['max'] if 'max' in pattern else len(word_list)

        if prev_match is None:
            # try to match at_most repeats of pattern
            sub_results = [None] * at_most
            i = 0
            max_i = 0
            max_sub_results = []
            while i >= 0 and i < at_most:
                word_list_offset = sum([s[0] for s in sub_results[:i]])
                sub_results[i] = matches_beginning(sub_pattern,
                                                   word_list[word_list_offset:],
                                                   sub_results[i])

                i += 1 if sub_results[i] is not None else -1

                if i > max_i:
                    max_i = i
                    max_sub_results = sub_results[:i].copy()

            if at_least <= 0:
                max_sub_results = [(0, None)] + max_sub_results
                    
            if max_sub_results and len(max_sub_results) >= at_least:
                return sum([s[0] for s in max_sub_results]), max_sub_results
            else:
                return None
        elif len(prev_match[1]) > 1:
            return sum([s[0] for s in prev_match[1][:-1]]), prev_match[1][:-1]
        else:
            return None

    elif isinstance(pattern, str):
        # +, *, or regular expression
        if pattern == '*':
            to_index = 0 if prev_match is None else prev_match[0] + 1
            if to_index <= len(word_list) or (to_index == 1 and len(word_list) == 0):
                return to_index,
        elif pattern == '+':
            to_index = 1 if prev_match is None else prev_match[0] + 1
            if to_index <= len(word_list):
                return to_index,
        elif prev_match is None and word_list and re.fullmatch(pattern, word_list[0]):
            return 1,
        else:
            return None
    else:
        raise ValueError("Illegal pattern type '{}': {}".format(type(pattern), pattern))


def write_data(sentences, output_file_name):
    with open(output_file_name, 'w') as f:
        for sentence in sentences:
            for word, label in zip(sentence.words, sentence.labels):
                if label is None:
                    label = 'O'
                f.write("{}\t{}\n".format(word, label))
            f.write("\n")


def label_data(sentences):
    CARDINALS_2_9 = ['two|three|four|five|six|seven|eight|nine']
    CARDINALS_1_9 = ['one|' + CARDINALS_2_9[0]]
    CARDINALS_10_19 = ['ten|eleven|twelve|thirteen|fourteen|fifteen|sixteen|seventeen|eighteen|nineteen']
    CARDINALS_TENS_20_90 = ['twenty|thirty|forty|fifty|sixty|seventy|eighty|ninety']
    
    ORDINALS_1_9 = ['first|second|third|fourth|fifth|sixth|seventh|eighth|ninth']
    ORDINALS_10_19 = ['tenth|eleventh|twelfth|thirteenth|fourteenth|fifteenth|sixteenth|seventeenth|eighteenth|nineteenth']
    ORDINALS_TENS_20_90 = ['twentieth|thirtieth|fortieth|fiftieth|sixtieth|seventieth|eightieth|ninetieth']

    CARDINALS_NOT_TENS_20_99 = [[x, y] for x in CARDINALS_TENS_20_90[0].split('|') for y in CARDINALS_1_9[0].split('|')]
    ORDINALS_NOT_TENS_20_99 = [[x, y] for x in CARDINALS_TENS_20_90[0].split('|') for y in ORDINALS_1_9[0].split('|')]

    CARDINALS_1_99 = CARDINALS_1_9 + CARDINALS_10_19 + CARDINALS_TENS_20_90 + CARDINALS_NOT_TENS_20_99
    ORDINALS_1_31 = ORDINALS_1_9 + ORDINALS_10_19 + [['twenty', y] for y in ORDINALS_1_9] +\
                    ['twentieth|thirtieth'] + [['thirty', 'first']]
    ORDINALS_1_99 = ORDINALS_1_9 + ORDINALS_10_19 + ORDINALS_TENS_20_90 + ORDINALS_NOT_TENS_20_99
    
    # ---------------- [DATE] --------------- #
    # # DEBUG: redefine sentences
    # sentences = [Sentence(words.split(' ')) for words in
    #              [
    #                  "I suggest Friday afternoon .",
    #                  "Next Monday or next year .",
    #                  "How about tomorrow ?",
    #                  "Let's meet the day after .",
    #                  "How would the twenty fifth work for you ?",
    #                  "some time in May .",
    #                  "January twenty twenty",
    #                  "The fourth of July .",
    #                  "To me, the eleventh would be better .",
    #                  "I could do either a day earlier or a day later .",
    #                  "How about two o' clock on Tuesday ?",
    #                  "Some time in week forty two .",
    #                  "Any day next week would be fine .",
    #                  "Any time on Friday afternoon would work for me .",
    #                  "How about some time in the afternoon on Wednesday ?"
    #              ]]

    weekdays = ('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday')

    months = ('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August',
              'September', 'October', 'November', 'Dezember')

    named_days = ([(['New', "Year's"],
                    [([{'?':'Martin'}, 'Luther', 'King'],
                      ['M', 'L', 'K']), {'?':'Junior'}],
                    "President's|Mother's|Memorial|Father's|Independence|Labor|Cabrini",
                    "Columbus|Veterans|Christmas|Boxing|Ascension|Assumption|Reformation",
                    ["Indigenous", "People's"],
                    ["German", "Reunification"]),
                   "Day"],
                  ["New", "Year's"],
                  ["New", "Year's", "Eve"],
                  ["Christmas", "Eve"],
                  ["Good", "Friday"],
                  ["Maundy", "Thursday"],
                  ["Easter", ("Saturday", "Sunday", "Monday")],
                  ["Whit", "Monday"],
                  ["Corpus", "Christi"],
                  ["Day", "of", "German", "Unity"],
                  "Thanksgiving|Christmas|Epiphany|Carnival|Easter|Pentecost|Helloween")

    date = (['the', tuple(ORDINALS_1_31), {'?':['of', months]}], [months, tuple(ORDINALS_1_31)])
    
    relative = 'previous|next|same|following|preceding'

    absolute_days = ['on', (weekdays, named_days, date)]
    
    relative_weekdays = ([{'?':'the'}, relative, weekdays],
                         [weekdays, ('week', ['in', 'a', 'week'],
                                     ['in', tuple(CARDINALS_2_9), 'weeks'])])
    
    relative_days = ('yesterday|today|tomorrow',
                     ['a', ('day|month|year', weekdays), 'earlier|later'],
                     ['the', ('day|month|year', weekdays), 'before|after',
                      {'?':('yesterday|tomorrow', weekdays, named_days, date)}],
                     ['the', 'previous', ('day|month|year', weekdays)],
                     [(tuple(CARDINALS_1_99), 'some|several', ['a', 'number', 'of']),
                      'days|weeks|month', 'from', '.+'])

    relative_vague = [{'?':[('another', 'the', 'any', 'some'), ('days?', weekdays)]},
                      (['in', (['week', tuple(CARDINALS_1_99)],
                               [{'?':['the', tuple(ORDINALS_1_9), 'week', 'of']}, months])],
                       [(['[io]n', 'the', relative], 'next'),
                        ('week', 'month', 'year')])]

    date_patterns = (absolute_days, relative_days, relative_weekdays, relative_vague,
                     weekdays, named_days, months, date)

    # ---------------- [TIME] --------------- #

    # [absolute]
    # - a quarter past/to
    # - five/ten (minutes) to/past
    # - five fifteen
    # - 2 p.m / a.m
    # - 10 in the morning/afternoon/evening
    # - 12 noon
    # - at noon/midnight
    # - 2 o'clock (in the morning/...)
    # - tomorrow morning
    # - the morning after (DATE)
    # - Wednesday afternoon
    # - in the morning/the afternoon/mid-day
    # - "around", "no earlier/later than", "before/after", "sharp"
    # - "before lunch", "before dinner"
    # - between X and Y
    # [relative]
    # - an hour/two hours/ten minutes later/earlier
    # - a little later
    # - in two (and a half) hours

    # DEBUG: redefine sentences
    # sentences = [Sentence(words.split(' ')) for words in
    #              [
    #                  "I suggest Friday afternoon .",
    #                  "I suggest four on Friday afternoon .",
    #                  "Let's meet at twenty after two , okay ?",
    #                  "It's already five minutes past the hour .",
    #                  "How about quarter past ?",
    #                  "Should we say two p. m. ?",
    #                  "I'll be at your house at exactlt six twenty , so be ready .",
    #                  "Please be ready by noon .",
    #                  "It 's four o' clock in the morning .",
    #                  "How does ten to five in the afternoon sound to you ?",
    #                  "ten o' clock at night",
    #                  "I'll see you at twelve noon then .",
    #                  "I could either do one o' clock or five in the evening .",
    #                  "The last meeting is at midnight .",
    #                  "in the morning",
    #                  "the morning after",
    #                  "I had a great breakfast and after breakfast , I met Tim ."
    #              ]]
    # sentences[0].labels[2] = 'B-DATE'
    # sentences[1].labels[4] = 'B-DATE'
    

    CARDINALS_1_12 = (CARDINALS_1_9[0], '|'.join(CARDINALS_10_19[0].split('|')[:3]))
    CARDINALS_13_24 = ('|'.join(CARDINALS_10_19[0].split('|')[3:]),
                       'twenty', CARDINALS_NOT_TENS_20_99[:4])
    CARDINALS_1_24 = (CARDINALS_1_9[0], CARDINALS_10_19[0], 'twenty', CARDINALS_NOT_TENS_20_99[:4])
    CARDINALS_1_59 = tuple(CARDINALS_1_99[:39])

    # two o'clock etc.
    clock_time = ([CARDINALS_1_12, (['a.', 'm.'], ['p.', 'm.'],
                                    [{'pattern':["o'", 'clock'], 'max':1},
                                     (['in', 'the', 'morning'],
                                      ['in', 'the', 'afternoon'],
                                      ['in', 'the', 'evening'],
                                      [('at', ['in', 'the']), 'night'])])],
                  [CARDINALS_1_24, ["o'", 'clock', {'pattern':'sharp', 'max':1}]])

    named_point_in_day = ("midnight",
                          [{'pattern':['twelve', {'pattern':["o'", 'clock'], 'max':1}],
                            'max':1}, "noon"],
                          "mid-day")

    time_span = ([CARDINALS_1_59, {'pattern':'minutes', 'max':1}],
                 [('any', 'some'), 'time'],
                 [{'pattern':'a', 'max':1}, "quarter"],
                 [{'pattern':'half', 'max':1}, 'an', 'hour'])

    # define the patterns
    time_patterns = ([{'pattern':[{'pattern':time_span, 'max':1},
                                  ('before|at|after|to|past', 'in|on',
                                   ['earlier|later', 'than'],
                                   [{'pattern':'at', 'max':1}, 'around'])], 'max':1},
                      (clock_time,

                       # midnight, noon, mid-day
                       named_point_in_day,

                       # five fifteen
                       [CARDINALS_1_24, CARDINALS_1_59],

                       # parts of a day
                       [{'pattern':'the', 'max':1},
                        "morning|afternoon|evening|night"],

                       # five minutes past the hour
                       ['the', 'hour'])],

                     # before two
                     [{'pattern':time_span, 'max':1},
                      ('before|at|after|to|past', 'in|on',
                       ['earlier|later', 'than'],
                       [{'pattern':'at', 'max':1}, 'around']),
                      (CARDINALS_1_24, 'that')],

                     # a quarter past
                     [time_span, 'before|after|to|past|later|earlier'],
                     
                     # meals as reference points
                     [('before|at|over|after',
                       ['earlier|later', 'than'],
                       [{'pattern':'at', 'max':1}, 'around']),
                           
                      (CARDINALS_1_24,
                       ['breakfast|lunch|dinner|tea', {'pattern':'time', 'max':1}])],

                     # between X and Y
                     ["between",
                      (clock_time,

                       # midnight, noon, mid-day
                       named_point_in_day,

                       # five fifteen
                       [CARDINALS_1_24, {'pattern':CARDINALS_1_59, 'max':1}],

                       # parts of a day
                       [{'pattern':'the', 'max':1},
                        "morning|afternoon|evening|night"]),
                      "and",
                      (clock_time,

                       # midnight, noon, mid-day
                       named_point_in_day,

                       # five fifteen
                       [CARDINALS_1_24, {'pattern':CARDINALS_1_59, 'max':1}],

                       # parts of a day
                       [{'pattern':'the', 'max':1},
                        "morning|afternoon|evening|night"])])

    # apply the patterns
    for sentence in tqdm(sentences):
        label_added = False
        word_list = [w.lower() for w in sentence.words]
        
        for label, pattern in [('DATE', date_patterns), ('TIME', time_patterns)]:
        #for label, pattern in [('DATE', date_patterns)]:
            matches = all_matches(pattern, word_list, allow_overlap=True, pattern_to_lower=True)
            if matches is not None:
                for match in matches:
                    # make sure the matching words are not already annotated
                    if list(filter(lambda x: x is not None, sentence.labels[match[0]:match[1]])):
                        index = match[0] + 1
                        continue
                    # annotate with 'BIO' scheme (except the 'O' of course)
                    sentence.labels[match[0]] = 'B-' + label
                    sentence.labels[match[0]+1:match[1]] = ['I-' + label] * (match[1] - match[0] - 1)
                    label_added = True

        # additional TIME annotations: 'Friday morning', 'tomorrow afternoon'
        pre_matches = all_matches(([('any', 'some'), 'time'], CARDINALS_1_12),
                                  word_list, pattern_to_lower=True)
        for i, label in enumerate(sentence.labels):
            # find beginnings of DATE annotations
            if label == 'B-DATE':
                # check if there is a number followed by a preposition
                # immediately before the date
                if pre_matches and i > 0:
                    k = i - 1 if word_list[i - 1] in ['on', 'at'] else i
                    for match in pre_matches:
                        if match[1] == k:
                            # make sure the matching words are not already annotated
                            if list(filter(lambda x: x is not None, sentence.labels[match[0]:match[1]])):
                                continue
                            # annotate with 'BIO' scheme (except the 'O' of course)
                            sentence.labels[match[0]] = 'B-TIME'
                            sentence.labels[match[0]+1:match[1]] = ['I-TIME'] * (match[1] - match[0] - 1)
                            label_added = True

                            

                # find the end of the DATE annotation
                for j, label in enumerate(sentence.labels[i+1:]):
                    if label != 'I-DATE':
                        index = i + j + 1
                        match = first_match("morning|afternoon|evening|night", word_list[index:])
                        if match:
                            match = (match[0] + index, match[1] + index)
                            sentence.labels[match[0]] = 'B-TIME'
                            sentence.labels[match[0]+1:match[1]] = ['I-TIME'] * (match[1] - match[0] - 1)
                            label_added = True

        #print("-" if label_added else 'o', sentence)


def generate_test(letters, depth=0):
    pattern_type = random.randint(0, 7)
    word_list = []

    if pattern_type < 3:
        pattern = []
        
        num_subsets = max(1, int(round(random.gauss(2.5 - depth, 1))))
        when_extend = [random.randint(0, num_subsets-1)] if pattern_type == 0 else list(range(num_subsets))
        for i in range(num_subsets):
            sub_pattern, sub_word_list = generate_test(letters, depth+1)

            pattern.append(sub_pattern)
            if i in when_extend:
                word_list.extend(sub_word_list)

        if pattern_type == 0:
            # set
            pattern = tuple(pattern)
    else:
        # string
        letter_index = random.randint(0, len(letters) + 1)
        if letter_index == len(letters) + 1:
            pattern = '*'
            word_list = random.choices(letters, k=max(0, random.randint(-1, 3)))
        elif letter_index == len(letters):
            pattern = '+'
            word_list = random.choices(letters, k=random.randint(1, 5))
        else:
            pattern = letters[letter_index]
            word_list = [letters[letter_index]]

    return pattern, word_list
    
            
def test_rule_engine():
    TESTS = [
        (True, {'pattern':'a', 'min':1, 'max':2}, ['a']),
        (False, {'pattern':'a', 'min':2, 'max':2}, ['a']),
        (True, {'pattern':'a', 'min':2, 'max':2}, ['a', 'a']),
        (True,  '*', []),
        (True,  ['a'], ['a']),
        (True,  ['a', 'b'], ['a', 'b']),
        (True,  ['a', 'b', 'c'], ['a', 'b', 'c']),
        (True,  ['a', 'b'], ['a', 'a', 'b']),
        (False, [['a'], 'b'], ['b']), # SHOULD FAIL
        (True,  [['a'], 'b'], ['a', 'b']),
        (True,  [['a'], 'b'], ['a', 'a', 'b']),
        (True,  [['a', 'b'], 'c'], ['a', 'b', 'c']),
        (True,  [['b', 'c'], 'd'], ['a', 'b', 'c', 'd']),
        (True,  [['b', '*'], 'c'], ['a', 'b', 'c', 'd']),
        (False, [['b', '+'], 'c'], ['a', 'b', 'c', 'd']), # SHOULD FAIL
        (True,  [['b', '*'], 'd'], ['a', 'b', 'c', 'd']),
        (True,  ['a', '*'], ['a']),
        (False, ['a', '+'], ['a']), # SHOULD FAIL
        (False, [(['a', 'b'],)], ['a']), # SHOULD FAIL
        (False, [(['a', 'b'],)], ['b']), # SHOULD FAIL
        (True,  [(['*'], 'A'), 'B', '*'], ['A', 'B', 'A', 'B']),
        (True,  ['a', 'd', [['+'], '*', 'c'], 'f'], ['a', 'd', 'a', 'a', 'b', 'c', 'e', 'c', 'f']),
        (True,  ['*', (['+'], ('*',))], [])
    ]

    terminal_colors = { 'GREEN':'\033[32m', 'RED':'\033[31m', 'NO_COLOR':'\033[0m' }

    for test_condition in TESTS:
        assert len(test_condition) == 3, test_condition
        expect_match = test_condition[0]
        test = test_condition[1:]
        assert len(test) == 2, test
        match = first_match(*test)
        color = terminal_colors['GREEN' if (expect_match and match is not None) or (not expect_match and match is None) else 'RED']
        print("{}".format(color) +\
              "{} {}".format('PASS' if match is not None else 'FAIL', test) +\
              "{}".format(terminal_colors['NO_COLOR']))
        

    if DEBUG_RULE_ENGINE:
        return
    
    # generate some tests automatically
    print("---------------------")
    import datetime
    now = datetime.datetime.now()

    ten_letters = [chr(x) for x in range(ord('a'), ord('a') + 11)]
    while (datetime.datetime.now() - now).seconds < 180:
        num_letters = random.randint(1, 7)
        test = generate_test(ten_letters[:num_letters])
        assert len(test) == 2, test
        if not first_match(*test):
            print("FAIL", test)
            return

    print("PASSed all tests.")
    

if __name__ == '__main__':
    if len(sys.argv) != 2 and len(sys.argv) != 3:
        print("USAGE: python3 {} <input.tsv> <output.tsv>".format(sys.argv[0]))
        sys.exit(1)

    # test_rule_engine()

    in_file = sys.argv[1]
    out_file = sys.argv[2] if len(sys.argv) == 3 else '_date_time'.join(os.path.splitext(in_file))

    print("Reading", in_file)
    sentences = read_data(in_file)
    print("Removing 'O' labels")
    for sentence in sentences:
        for i in range(len(sentence.labels)):
            if sentence.labels[i] == 'O':
                sentence.labels[i] = None
    print("Applying labeling rules")
    label_data(sentences)
    print("Writing", out_file)
    write_data(sentences, out_file)
