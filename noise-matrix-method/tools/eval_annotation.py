# Copyright 2020 Saarland University, Spoken Language Systems LSV
# Authors: Lukas Lange, Michael A. Hedderich, Dietrich Klakow, Thomas Kleinbauer
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
# THIS CODE IS PROVIDED *AS IS*, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
# WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
# MERCHANTABLITY OR NON-INFRINGEMENT.
#
# See the Apache 2 License for the specific language governing permissions and
# limitations under the License.

import sys

from date_time_rules import read_data as read_data

if __name__ == '__main__':
    if len(sys.argv) != 3 and len(sys.argv) != 4:
        print("USAGE: python {} <true-annotations> <predictions> [<label-bodies>]".format(sys.argv[0]))
        sys.exit(1)

    # define the labels to evaluate here
    if len(sys.argv) == 4:
        label_bodies = sys.argv[3].split(',')
    else:
        label_bodies = ['PER', 'ORG', 'LOC', 'DATE', 'TIME']
    labels = [label for body in label_bodies for label in ['B-{}'.format(body), 'I-{}'.format(body)]]

    # read ground truth and predicted annotations
    ground_truth = read_data(sys.argv[1])
    predictions = read_data(sys.argv[2])

    # make sure that they're actually annotating the same data
    if len(ground_truth) != len(predictions):
        print("True annotations and predictions contain different numbers of sentences.")
        sys.exit(1)

    # count matches
    counts = {label:{} for label in labels}
    counts['O'] = {}

    total_word_count = 0
    num_correct = 0
    for i, (gt, pr) in enumerate(zip(ground_truth, predictions)):
        if gt.words != pr.words:
            print("Sentence #{} differs between ground truth vs. predictions.".format(i))
            sys.exit(1)

        total_word_count += len(gt.words)
            
        for gt_label, pr_label in zip(gt.labels, pr.labels):
            if pr_label is None:
                pr_label = 'O'

            if gt_label == pr_label:
                num_correct += 1
                
            gt_counts = counts[gt_label]
            if pr_label == gt_label:
                gt_counts['tp'] = gt_counts.get('tp', 0) + 1
            else:
                gt_counts['fn'] = gt_counts.get('fn', 0) + 1

                pr_counts = counts[pr_label]
                pr_counts['fp'] = pr_counts.get('fp', 0) + 1

            for label in labels:
                if label != gt_label:
                    label_counts = counts.get(label, {})
                    label_counts['tn'] = label_counts.get('tn', 0) + 1

    print("|Label |Precision| Recall |F-score |")
    print("+------+---------+--------+--------+")

    sum_precisions = 0
    sum_recalls = 0
    sum_fscores = 0
    sum_tps = 0
    sum_fps = 0
    sum_fns = 0

    for label in labels + ['O']:
        label_counts = counts[label]

        tp = label_counts['tp'] if 'tp' in label_counts else 0
        fp = label_counts['fp'] if 'fp' in label_counts else 0
        fn = label_counts['fn'] if 'fn' in label_counts else 0

        label_precision = tp / (tp + fp) if tp + fp != 0 else 0
        label_recall = tp / (tp + fn) if tp + fn != 0 else 0
        label_fscore = 2 * tp / (2 * tp + fp + fn) if tp + fp + fn != 0 else 0

        print("|{:6}| {:04f}|{:04f}|{:04f}|".format(label,
                                                    label_precision, label_recall, label_fscore))

        sum_precisions += label_precision
        sum_recalls += label_recall
        sum_fscores += label_fscore

        sum_tps += tp
        sum_fps += fp
        sum_fns += fn
        

    print("+------+---------+--------+--------+")
    print("|Macro:| {:04f}|{:04f}|{:04f}|".format(sum_precisions / (len(labels) + 1),
                                                  sum_recalls / (len(labels) + 1),
                                                  sum_fscores / (len(labels) + 1)))
    print("|Micro:| {:04f}|{:04f}|{:04f}|".format(sum_tps / (sum_tps + sum_fps),
                                                  sum_tps / (sum_tps + sum_fns),
                                                  2 * sum_tps / (2 * sum_tps + sum_fps + sum_fns)))

    print()
    print("Total Accuracy: {:.3f}%".format(100 * num_correct / total_word_count))

