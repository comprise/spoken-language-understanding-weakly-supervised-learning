# Copyright 2020 Saarland University, Spoken Language Systems LSV
# Authors: Lukas Lange, Michael A. Hedderich, Dietrich Klakow, Thomas Kleinbauer
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
# THIS CODE IS PROVIDED *AS IS*, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
# WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
# MERCHANTABLITY OR NON-INFRINGEMENT.
#
# See the Apache 2 License for the specific language governing permissions and
# limitations under the License.

import numpy as np
import sys
from tqdm import tqdm

class Sentence:
    def __init__(self, words, labels):
        assert words, "Words must not be empty or None."
        assert labels, "Words must not be empty or None."
        assert len(words) == len(labels), "The same number of words and labels must be given."
        
        self.words = words
        self.poss = [None] * len(words)
        self.clean_labels = labels
        self.noisy_labels = [None] * len(labels)
        

def read_data(input_file_name):
    sentences = []
    words = []
    labels = []

    with open(input_file_name, 'r') as f:
        for line_number, line in enumerate(f):
            split = line.strip().split('\t')

            if len(split) == 0:
                if words:
                    sentences.append(Sentence(words, labels))
                    words = []
                    labels = []
            elif len(split) < 3:
                words.append(split[0])
                labels.append(split[1] if len(split) == 2 else None)
            else:
                print("Cannot parse file '{}':".format(input_file_name))
                print("[{}]: {}".format(line_number, line))
                sys.exit(2)

    if words:
        sentences.append(Sentence(words, labels))

    return sentences


if __name__ == '__main__':
    if len(sys.argv) != 3:
        print("USAGE: python {} <clean-in.tsv> <noisy-out.tsv>".format(sys.argv[0]))
        sys.exit(1)

    in_file = sys.argv[1]
    out_file = sys.argv[2]


    # initialize the random state
    sampling_state = np.random.RandomState(12) # the same value as in the .json file!

    # -- read the data: words and clean labels --
    sentences = read_data(in_file)
    total_word_count = sum([len(s.words) for s in sentences])

    # collect all labels
    labels = set()
    for sentence in sentences:
        for label in sentence.clean_labels:
            labels.add(label)
    labels = list(labels)

    # restrict clean data to 1%
    clean_word_count = int(total_word_count * 0.01)
    clean_indices = (np.arange(clean_word_count) + sampling_state.randint(total_word_count)) % total_word_count

    # -- create the distributions p(label) --
    # create counts
    label_counts = { l:1 for l in labels } # Laplace smoothing
    i = 0
    for sentence in sentences:
        if i > clean_indices[-1]:
            break
        if i + len(sentence.clean_labels) < clean_indices[0]:
            i += len(sentence.clean_labels)
            continue

        for label in sentence.clean_labels:
            if i >= clean_indices[0] and i <= clean_indices[-1]:
                label_counts[label] = label_counts.get(label, 0) + 1
            i += 1
    
    # normalize counts to probabilities
    label_dist = [label_counts[label] / (clean_word_count + len(labels)) for label in labels]
            
    # -- annotate the words by sampling --
    p_correct = 0
    different_labels_count = 0
    for sentence in sentences:
        for i, pos in enumerate(sentence.poss):
            # choose a label at random according to the distribution
            # computed above
            p = label_dist

            p_correct += p[labels.index(sentence.clean_labels[i])]

            label = np.random.choice(labels, p=p)
            sentence.noisy_labels[i] = label

            if sentence.clean_labels[i] != label:
                different_labels_count += 1

    # -- evaluate noise
    # compute maximum possible noise level
    p_correct /= total_word_count
    print("Expected noise level: {:.5f}".format(1 - p_correct))
    print("Actual noise level:   {:.5f}".format(different_labels_count / total_word_count))

    # -- write data --
    with open(out_file, 'w') as f:
        for sentence in sentences:
            for word, label in zip(sentence.words, sentence.noisy_labels):
                f.write("{}\t{}\n".format(word, label))
            f.write("\n")
