# Copyright 2020 Saarland University, Spoken Language Systems LSV
# Authors: Lukas Lange, Michael A. Hedderich, Dietrich Klakow, Thomas Kleinbauer
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
# THIS CODE IS PROVIDED *AS IS*, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
# WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
# MERCHANTABLITY OR NON-INFRINGEMENT.
#
# See the Apache 2 License for the specific language governing permissions and
# limitations under the License.

import sys

from date_time_rules import Sentence
from date_time_rules import read_data

if __name__ == '__main__':
    if len(sys.argv) < 3:
        print("USAGE: python {} <file1.tsv> <file2.tsv> [<file3.tsv>...]".format(sys.argv[0]))
        sys.exit(1)

    # read all files
    annotations = [read_data(file_name) for file_name in sys.argv[1:]]

    print(*[len(a) for a in annotations])
    
    # make sure all files are compatible with each other
    for i, annotation in enumerate(annotations[1:]):
        if len(annotation) != len(annotations[0]):
            raise ValueError("Different number of sentences in {} and {}".format(sys.argv[1], sys.argv[i+1]))

        for s1, s2 in zip(annotations[0], annotation):
            if s1.words != s2.words:
                raise ValueError("Different sentences in {} and {}".format(sys.argv[1], sys.argv[i+1]))

    # create the output annotations
    master = []
    for sentences in zip(*annotations):
        master.append(Sentence(sentences[0].words))

        for i in range(len(sentences[0].words)):
            label_set = set([s.labels[i] for s in sentences])
            if 'O' in label_set:
                label_set.remove('O')
            if None in label_set:
                label_set.remove(None)

            if len(label_set) == 0:
                master[-1].labels.append('O')
            elif len(label_set) == 1:
                master[-1].labels.append(next(iter(label_set)))
            else:
                #raise ValueError("Cannot merge conflicting annotations:", label_set, sentences[0].words)
                master[-1].labels.append(sentences[0].labels[i])


    # write result to 'merged.tsv'
    with open('merged.tsv', 'w') as f:
        for sentence in master:
            for word, label in zip(sentence.words, sentence.labels):
                f.write("{}\t{}\n".format(word, label))
            f.write("\n")
        
                
        
