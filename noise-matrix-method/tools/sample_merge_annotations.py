# Copyright 2020 Saarland University, Spoken Language Systems LSV
# Authors: Lukas Lange, Michael A. Hedderich, Dietrich Klakow, Thomas Kleinbauer
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
# THIS CODE IS PROVIDED *AS IS*, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
# WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
# MERCHANTABLITY OR NON-INFRINGEMENT.
#
# See the Apache 2 License for the specific language governing permissions and
# limitations under the License.

import random
import sys

from sample_noise import Sentence, read_data

if __name__ == '__main__':
    if len(sys.argv) != 5 and len(sys.argv) != 6:
        print("USAGE: python3 {} <clean.tsv> <noisy.tsv> <noise-level> <merged.tsv> [<force-clean-interval>]".format(sys.argv[0], ))
        sys.exit(1)

    sentences = read_data(sys.argv[1])
    noisy_sentences = read_data(sys.argv[2])

    desired_noise_level = float(sys.argv[3])

    outfile = sys.argv[4]

    if len(sys.argv) == 6:
        fromto = sys.argv[5].split(':')
        if len(fromto) != 2:
            print("Format of <force-clean-interval> must be <from>:<to>.")
            sys.exit(1)
        clean_interval = (int(fromto[0]), int(fromto[1]))
    else:
        clean_interval = (0,-1)

    # 'read_data' above actually stored the noisy labels in the
    # 'clean_labels' field of the returned sentences. Instead, let's
    # combine clean and noisy annotations into the same data
    # structure, using the correct fields.
    if len(sentences) != len(noisy_sentences):
        print("ERROR: clean and noisy data do not contain the same number of sentences.")
        sys.exit(1)

    for i, (clean_sentence, noisy_sentence) in enumerate(zip(sentences, noisy_sentences)):
        if clean_sentence.words != noisy_sentence.words:
            print("ERROR: Sentences #{} differ in clean and noisy data:".format(i))
            print(" ".join(clean_sentence.words))
            print(" ".join(noisy_sentence.words))
            sys.exit(1)

        clean_sentence.noisy_labels = noisy_sentence.clean_labels

    noisy_sentences = None

    # compute noise level
    total_word_count = 0
    different_labels = []
    k = 0
    for i,sentence in enumerate(sentences):
        total_word_count += len(sentence.words)
        for j, (clean_label, noisy_label) in enumerate(zip(sentence.clean_labels, sentence.noisy_labels)):
            if clean_label != noisy_label and (k < clean_interval[0] or k > clean_interval[1]):
                different_labels.append((i, j))
            k += 1

    noise_level = len(different_labels) / total_word_count

    # make sure the desired noise level is achievable
    if desired_noise_level > noise_level:
        print("Note: noise level {} not achievable, merging at {} instead.".format(desired_noise_level, noise_level))
        desired_noise_level = noise_level


    # randomly choose which noisy labels to use
    if desired_noise_level == noise_level:
        noisy_merge_labels = set(different_labels)
    else:
        noisy_merge_labels = set(random.sample(different_labels, int(round(desired_noise_level * k))))

    # write the output file
    k = 0
    with open(outfile, 'w') as f:
        for i, sentence in enumerate(sentences):
            for j, word in enumerate(sentence.words):
                f.write("{}\t".format(word))
                if (i,j) in noisy_merge_labels and (k < clean_interval[0] or k > clean_interval[1]):
                    f.write("{}\n".format(sentence.noisy_labels[j]))
                else:
                    f.write("{}\n".format(sentence.clean_labels[j]))
                k += 1
            f.write("\n")
                    
