This is the Weakly Supervised Learning Library for Text Processing of
the EU H2020 Project "COMPRISE". Please consult the file
"acknowledgment.pdf"

The code in this repository is one half of the initial version of this
software library, the other half addressing weakly supervised learning
for speech-to-text processing.

A detailed description of the library can be found in the official
project deliverable "D4.2 Initial weakly supervised learning library",
available at https://www.compriseh2020.eu

Installation instructions can be found in the file "doc.pdf" contained
in this directory.
