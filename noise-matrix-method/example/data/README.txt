The data found in this directory consists of three files:

- train.tsv
- valid.tsv
- test.tsv

The contents are English sentences annotated with the following Named
Entity labels:

- PER (Persons)
- ORG (Organizations)
- LOC (Locations)
- DATE (Dates)
- TIME (Temporal expressions)
- O (Everything else)

The sentences are encoded with one word per line, followed by a TAB
character ('\t'), followed by one of the six labels listed above.
Sentence boundaries are marked by empty lines.

The sentences themselves are an excerpt from

  Dorothy Wordsworth: "Recollections of a tour made in Scotland A.D. 1803",
  J. C. Shairp (Ed.), 1874

The three files listed above constitute a training / development /
test split containing 54305 / 15544 / 7759 words respectively, which
implements roughly a 70 / 20 / 10 percent of the total number of
words.


