Prerequisites
=============

Before you can run this example, you have to make sure that you have
successfully installed all required dependencies. Please follow the
installation instructions in ../doc.pdf for this.

How to run
==========

All of the following commands need to be run from inside the 'example'
directory.

1) Create noisy annotations for the training data

In order to create noisy annotations based on part-of-speech tags and
taking the previous NER label into account, run the following command:

  python3 ../tools/sample_noise_prev_pos.py data/train_clean.tsv data/train_noisy.tsv

Only 1% of the clean data is used to estimate the probability
distributions from which to sample the noisy annotations.

2) Convert .tsv files to .pickle files

All data files, which are provided in TAB-separated-values format
(.tsv), need to be converted to the .pickle format expected by the
classifier. This needs to be done only once and will speed up the
run-time of the classifier.

  python3 ../tools/create_pickle.py verbmobil io data/train_clean.tsv data/train_clean.pickle

  python3 ../tools/create_pickle.py verbmobil io data/train_noisy.tsv data/train_noisy.pickle

  python3 ../tools/create_pickle.py verbmobil bio data/valid.tsv data/valid.pickle

  python3 ../tools/create_pickle.py verbmobil bio data/test.tsv data/test.pickle
  
Note that the training data is expected to be in IO format while
validation and test set must be in BIO format. This is reflected in
the different command line parameters passed in the first two vs. the
last two script invocations.

3) Run the classifier

In order to train and evaluate a model on the data suchly created, run
the following command:

  python3 ../main/ner.py --config-dir=. example

4) Inspect the result

The example data present a hard problem for the classifier: the label
distribution is extremely skewed, with the O-tag dominating all other
labels dramatically. Moreoever, we're estimating the noisy labels
using only one percent of the clean data.

Therefore, we observe only a modest result at the end of the training,
with f-scores in the mid-20% region. Also, it takes a high number
epochs until any training effect is visible on the validation data at
all, as can be witnessed in the regular print-outs during the training
phase.
