# Copyright 2020 Saarland University, Spoken Language Systems LSV
# Authors: Lukas Lange, Michael A. Hedderich, Dietrich Klakow, Thomas Kleinbauer
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
# THIS CODE IS PROVIDED *AS IS*, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
# WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
# MERCHANTABLITY OR NON-INFRINGEMENT.
#
# See the Apache 2 License for the specific language governing permissions and
# limitations under the License.

import argparse
import datetime
import importlib
import logging
import torch
from torch.utils import data

import numpy as np
from experimentalsettings import ExperimentalSettings
from conll_dataset import CoNLLDataset
from ner_datacode import LabelRepresentation
import utils
import sys

def init():
    # create and configure a logger
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)

    # check if a GPU is available
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

    # DEBUG
    # device = 'cpu'
    # print("!!!CAUTION -- NOT USING THE GPU FOR DEBUGGING PURPOSES!!!")
    
    return device, logger
    

def parse_command_line_args():
    # create a command line parser
    parser = argparse.ArgumentParser()

    def between_0_and_1(s):
        try:
            f = float(s)
            if f >= 0 and f <= 1:
                return f
        except ValueError:
            pass
        
        raise argparse.ArgumentTypeError('{} must be a value between 0 and 1'.format(s))
    
    # configure the command line parser
    parser.add_argument('config', metavar='<config>', help='the name of the experimental setting to use')
    parser.add_argument('--config-dir', help='the path to the directory containing the config file')
    parser.add_argument('--epochs', type=int, help='the number of epochs used in training')
    parser.add_argument('--sample-pct-clean', type=between_0_and_1, help='the percentage of clean data used in training')
    parser.add_argument('--sample-pct-noisy', type=between_0_and_1, help='the percentage of noisy data used in training')
    parser.add_argument('--use-identity-matrix', action='store_true', help='turns off the use of a prepopulated noise matrix')

    # do the actual parse of the command line arguments
    parse_result = parser.parse_args()

    # turn the parse result into a dict() with upper-case keys
    return { key.upper():value for key,value in vars(parse_result).items() if value is not None }


def load_config(command_line_args):
    # the different variations of neural networks available
    trainer_classes = { 'example': 'GlobalCMTrainer' }

    # load the json file specifying the configuration but allow
    # settings to be overwritten by command line switches
    load_json_parameters = [command_line_args['CONFIG']]
    load_json_parameters.append(command_line_args)
    if 'CONFIG_DIR' in command_line_args:
        load_json_parameters.append(command_line_args['CONFIG_DIR'])
    config = ExperimentalSettings.load_json(*load_json_parameters)

    # sanity check for all config values
    # TODO: do this for *all* values
    for key, cmd_line_option, valid_values in [#('SETTING', '--setting', trainer_classes.keys()),
                                               ('LABEL_FORMAT', None, ['io', 'bio'])]:
        if key not in config:
            if cmd_line_option:
                error_msg = "{} not specified - either use {} or put it in the config file.".format(key, cmd_line_option)
            else:
                error_msg = "{} not defined in config file.".format(key)
            raise ValueError(error_msg)

        if config[key] not in valid_values:
            raise ValueError("Unknown {} '{}', must be one of {}.".format(key, config[key], valid_values))

    # add the trainer to use to the config, based on the given setting.
    config['TRAINER'] = trainer_classes[command_line_args['CONFIG']]
    
    return config


def create_network(config, device):
    # determine which Trainer to instantiate and whether or not to
    # remove label prefixes
    trainer_class_name = config['TRAINER']
    embedding_vector_size = 300 # TODO: read from config!

    # DEBUG
    # label_representation = LabelRepresentation()
    # if config["LABEL_FORMAT"] == "io":
    #     label_representation.use_connl_io_labels()
    # else:
    #     label_representation.use_connl_bio_labels()
    label_representation = None

    # the data sets are added to the trainer at a later stage, not in
    # the initializer
    all_datasets = [None] * 4
        
    # ---- REFACTOR ---- #
    import model
    return model.GlobalCMTrainer(all_datasets, embedding_vector_size, label_representation, config, device)
    # ---- -------- ---- #

    # create a new Trainer instance according to the given
    # configuration
    trainer_module = importlib.import_module('Trainers.{}'.format(trainer_class_name))
    trainer_class = getattr(trainer_module, trainer_class_name)
    
    # TODO: label_representation.get_num_labels() could be done inside
    # Trainer.__init__, since label_representation is passed as an
    # argument, too
    # TODO: make 'embedding_vector_size' part of the config, and then
    # test if it matches the given embeddings
    # TODO: do we really need to pass 'config' here? And if so, can we
    # move the computation of the other parameters into
    # Trainer.__init__ ?
    return trainer_class(all_datasets, label_representation.get_num_labels(),
                         embedding_vector_size, label_representation, config,
                         None, device)


def load_datasets(config):
    train_clean, _, embedding_vector_size = utils.load_processed_data(config["PATH_TRAIN_CLEAN"])
    train_noisy, _, _ = utils.load_processed_data(config["PATH_TRAIN_NOISY"])
    
    dev, _, _ = utils.load_processed_data(config["PATH_DEV"])  # we always test on BIO scheme
    test, _, _ = utils.load_processed_data(config["PATH_TEST"])

    # create label_representation format from data
    io_format = False
    bio_format = False
    labels = set()

    for train_data in [train_clean, train_noisy]:
        for instance in train_data:
            labels.add(instance.label)
            if instance.label != 'O':
                if len(instance.label) > 1 and instance.label[:2] in ['B-', 'I-']:
                    bio_format = True
                else:
                    io_format = True

    if io_format and bio_format:
        print("ERROR: found mixed io/bio label format in training data. Aborting.")
        sys.exit(2)

    if not io_format and not bio_format:
        print("ERROR: neither io nor bio labels in training data. Aborting.")
        sys.exit(2)

    if config['LABEL_FORMAT'] != ('io' if io_format else 'bio'):
        print("ERROR: specified LABEL_FORMAT does not match training data. Aborting.")
        sys.exit(2)

    # maybe the dev or test data have additional labels (which would
    # suck but we want to be prepared)
    for data in [dev, test]:
        for instance in data:
            label = instance.label
            if instance.label != 'O':
                if len(label) > 1 and label[:2] in ['B-', 'I-']:
                    if config['LABEL_FORMAT'] == 'io':
                        label = label[2:]
                else:
                    print("ERROR: {} set is not in BIO format. Aborting.")
                    sys.exit(2)
            labels.add(label)

    # ensure a stable order of labels
    labels = list(labels)

    # install one-hot label embeddings into the training instances
    label_embeddings = {label:np.zeros(len(labels)) for label in labels}
    for i, label in enumerate(labels):
        label_embeddings[label][i] = 1

    for train_data in [train_clean, train_noisy]:
        for instance in train_data:
            instance.label_emb = label_embeddings[instance.label]

    # create a matching label representation in the trainer
    label_map = {label:i for i,label in enumerate(labels)} 
    label_representation = LabelRepresentation()
    label_representation.use_specific_label_map(label_map)

    return train_clean, train_noisy, dev, test, label_representation


def train(nn, train_data_clean, train_data_noisy, dev_data):
    nn.train_clean_full = train_data_clean
    nn.train_noisy_full = train_data_noisy
    nn.dev = dev_data
    
    nn.dev_dataset = CoNLLDataset(dev_data)
    nn.dev_dataloader = data.DataLoader(nn.dev_dataset, batch_size=config["BATCH_SIZE"], shuffle=False, num_workers=0)

    return nn.train()


def evaluate(nn, model, test_data, logger):
    test_dataset = CoNLLDataset(test_data)
    test_dataloader = data.DataLoader(test_dataset, batch_size=config["BATCH_SIZE"], shuffle=False, num_workers=0)

    # TODO: are all the parameters really necessary?
    logger.info(utils.long_evaluation(model, test_data, test_dataloader, nn.device, nn.label_representation, nn.SETTINGS))


if __name__ == '__main__':
    logging.info("Started at str({}).".format(datetime.datetime.now()))

    # initialize the general setup independent of any command line
    # parameters or config options
    device, logger = init()

    # the configuration is a combination of the parameters given on
    # the command line and a config file
    command_line_args = parse_command_line_args()
    config = load_config(command_line_args)
    logging.info("Configuration used: {}".format(config))

    # define the neural network structure
    nn = create_network(config, device)

    # load the train, development and test datasets
    train_data_clean, train_data_noisy, dev_data, test_data, lr = load_datasets(config)

    # install the label representation
    nn.label_representation = lr
    
    # train the network
    model = train(nn, train_data_clean, train_data_noisy, dev_data)

    # evaluate the network's performance on the test data
    evaluate(nn, model, test_data, logger)
    
    logging.info("Finished at str({}).".format(datetime.datetime.now()))
