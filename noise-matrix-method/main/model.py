# Copyright 2020 Saarland University, Spoken Language Systems LSV
# Authors: Lukas Lange, Michael A. Hedderich, Dietrich Klakow, Thomas Kleinbauer
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
# THIS CODE IS PROVIDED *AS IS*, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
# WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
# MERCHANTABLITY OR NON-INFRINGEMENT.
#
# See the Apache 2 License for the specific language governing permissions and
# limitations under the License.

import torch
from torch.utils import data
import numpy as np
import logging

from ner_datacode import Evaluation, LabelRepresentation


class CoNLLDataset(data.Dataset):
    def __init__(self, instances, onehot=False):
        self.instances = instances
        self.onehot = onehot

    def __len__(self):
        return len(self.instances)

    def __getitem__(self, index):
        ins = self.instances[index]

        emb = torch.from_numpy(ins.embedding).float()
        label = torch.from_numpy(ins.label_emb)
        if self.onehot:
            label = label.float()
        else:
            label = label.argmax()

        return emb, label, ins.word


class BaseModel(torch.nn.Module):
    def __init__(self, Feature_Extractor, hidden_size, dense1_out_size, dense_layer1_activation, num_labels):
        super(BaseModel, self).__init__()
        self.Feature_Extractor = Feature_Extractor
        self.dense_layer1 = torch.nn.Linear(hidden_size*2, dense1_out_size)
        if dense_layer1_activation == "relu":
            self.dense_layer1_activation = torch.nn.ReLU()
        else:
            self.dense_layer1_activation = torch.nn.Sigmoid()

        self.dense_layer2 = torch.nn.Linear(dense1_out_size, num_labels)

    def forward(self, x):
        feat = self.Feature_Extractor(x)
        out = self.dense_layer1(feat)
        out = self.dense_layer1_activation(out)
        out = self.dense_layer2(out)
        return out


class Feature_Extractor(torch.nn.Module):
    # A wrapper of LSTM for extracting text features
    def __init__(self, embedding_vector_size, hidden_size):
        super(Feature_Extractor, self).__init__()
        self.LSTM = torch.nn.LSTM(embedding_vector_size, hidden_size, batch_first=True, bidirectional=True)

    def forward(self, x):
        _, (hn, _) = self.LSTM(x, None)
        feat = torch.cat((hn[0, ::], hn[1, ::]), dim=1)
        return feat


class Global_CM(torch.nn.Module):
    def __init__(self, base_model, channel_weights, device):
        super(Global_CM, self).__init__()
        self.base_model = base_model
        # KLEIBA: the 'requires_grad' setting seems to make no difference?
        self.noise_mat = torch.tensor(channel_weights, requires_grad=True).float().to(device)
        self.activation1 = torch.nn.Softmax()
        self.activation2 = torch.nn.Softmax()

    def forward(self, x):
        out = self.base_model(x)
        out = torch.nn.functional.softmax(out, dim=1)
        out = torch.matmul(out, torch.nn.functional.softmax(self.noise_mat, dim=1))
        out = torch.log(out)
        return out


class GlobalCMTrainer():
    def __init__(self, all_datasets, embedding_vector_size, label_representation, SETTINGS, device):
        self.train_clean_full = all_datasets[0]
        self.train_noisy_full = all_datasets[1]
        self.train_clean = None
        self.train_noisy = None
        self.dev = all_datasets[2]
        # self.test = all_datasets[3]
        self.SETTINGS = SETTINGS
        self.sampling_state = np.random.RandomState(SETTINGS["SAMPLE_SEED"])
        self.batch_size = SETTINGS["BATCH_SIZE"]
        self.num_workers = SETTINGS["NUM_WORKERS"]
        self.use_noisy = SETTINGS["USE_NOISY"]
        self.lr = 0.001
        self.best_dev = -1 ## kleiba: necessary?
        # self.long_test_evaluation = None
        self.embedding_vector_size = embedding_vector_size
        self.label_representation = label_representation
        self.device = device

        self.dev_dataset = None
        self.dev_dataloader = None
        self.test_dataset = None
        self.test_dataloader = None
        

    def train(self):
        # create base model
        base_model, _ = self.create_BaseModel(self.embedding_vector_size,
                                              self.SETTINGS["LSTM_SIZE"], self.SETTINGS["DENSE_SIZE"],
                                              self.SETTINGS["DENSE_ACTIVATION"],
                                              self.label_representation.get_num_labels())
        
        base_model = base_model.to(self.device)
        base_loss = torch.nn.CrossEntropyLoss()
        base_optimizer = torch.optim.Adam(base_model.parameters(), lr=self.lr)

        c_loader, channel_weights = self.prepare_aligned_data()
        globalmodel = Global_CM(base_model, channel_weights, self.device)
        globalmodel_loss = torch.nn.NLLLoss()
        globalmodel_optimizer = torch.optim.Adam(globalmodel.parameters(), lr=self.lr)

        assert self.use_noisy

        report_interval = self.SETTINGS["REPORT_INTERVAL"]

        n_indices = np.arange(len(self.train_noisy))
        n_i = len(n_indices)

        for epoch in range(self.SETTINGS["EPOCHS"]):
            self.train_epoch(base_model, c_loader, base_loss, base_optimizer,
                             self.device, comment='clean')

            # select a random subset of self.train_noisy by
            if n_i >= len(n_indices):
                np.random.shuffle(n_indices)
                n_i = 0

            n_j = n_i + int(len(self.train_clean))
            n_subset_indices = n_indices[n_i:n_j]
            n_i = n_j

            # create a dataset instance from the chosen noisy data subset
            n_subdataset = CoNLLDataset(self.train_noisy[n_subset_indices,])
            n_loader = data.DataLoader(n_subdataset, batch_size=self.batch_size, shuffle=False,
                                       num_workers=self.num_workers)

            self.train_epoch(globalmodel, n_loader, globalmodel_loss, globalmodel_optimizer,
                             self.device, comment='noisy')

            if epoch % report_interval == 0:
                self.evaluate(base_model, epoch)

        return base_model


    def train_epoch(self, model, dataloader, loss_fn, optimizer, device, comment):
        model.train()
        
        for data in dataloader:
            xs, labels = data[0], data[1]
            xs = xs.to(device)
            labels = labels.to(device)

            optimizer.zero_grad()
            y_pred = model(xs)

            loss = loss_fn(y_pred, labels)
            loss.backward()
            optimizer.step()

 
    def prepare_aligned_data0(self):
        # pairs of clean and noisy labels are used to initalize the noise model
        train_size = int(len(self.train_clean_full) * self.SETTINGS["SAMPLE_PCT_CLEAN"])
        train_indices = np.arange(train_size) - self.sampling_state.randint(0, len(self.train_clean_full))
        #train_indices = np.sort(self.sampling_state.choice(np.arange(len(self.train_clean_full)), train_size))

        # we align the clean and noisy data for creating transition
        # matrices, or for building cleaning model
        self.train_clean = np.asarray(self.train_clean_full)[train_indices,]
        self.train_noisy = np.asarray(self.train_noisy_full)[train_indices,]

        # prepare clean data dataloader
        c_dataset = CoNLLDataset(self.train_clean)
        c_loader = data.DataLoader(c_dataset, batch_size=self.batch_size, shuffle=True, num_workers=self.num_workers)

        # compute noise_matrix / channel_weights
        if self.SETTINGS["USE_IDENTITY_MATRIX"]:
            noise_matrix = np.eye(self.label_representation.get_num_labels(),
                                  self.label_representation.get_num_labels())
            noise_matrix = noise_matrix * (1.0 - self.label_representation.get_num_labels() * 1e-8) + 1e-8
        else:
            noise_matrix = self.compute_noise_matrix(self.train_clean, self.train_noisy)

        # we assume that a small epsilon (1e-8) has already been added
        # e.g. inside compute_noise_matrix, so we can savely apply log
        # here
        channel_weights = np.log(noise_matrix)

        return c_loader, channel_weights


    def prepare_aligned_data(self):
        # the number of data points in self.train_clean and self.train_noisy
        clean_size = int(len(self.train_clean_full) * self.SETTINGS['SAMPLE_PCT_CLEAN'])
        noisy_size = int(len(self.train_noisy_full) * self.SETTINGS['SAMPLE_PCT_NOISY'])
        max_train_size = min(len(self.train_clean_full), len(self.train_noisy_full))

        # # non-sequential
        # if clean_size < noisy_size:
        #     noisy_indices = self.sampling_state.choice(np.arange(max_train_size), noisy_size)
        #     clean_indices = self.sampling_state.choice(noisy_indices, clean_size)
        # else:
        #     clean_indices = self.sampling_state.choice(np.arange(max_train_size), clean_size)
        #     noisy_indices = self.sampling_state.choice(clean_indices, noisy_size)

        # sequential
        if clean_size < noisy_size:
            clean_indices = (np.arange(clean_size) + self.sampling_state.randint(max_train_size)) % max_train_size
            remaining_noisy_indices = np.setdiff1d(np.arange(len(self.train_noisy_full)), clean_indices)
            noisy_indices = np.concatenate([clean_indices, self.sampling_state.choice(remaining_noisy_indices, noisy_size - clean_size)])
            print("Unique elements in noisy_indices:", len(set(noisy_indices)))
        else:
            noisy_indices = (np.arange(noisy_size) + self.sampling_state.randint(max_train_size)) % max_train_size
            remaining_clean_indices = np.setdiff1d(np.arange(len(self.train_clean_full)), noisy_indices)
            clean_indices = np.concatenate([noisy_indices, self.sampling_state.choice(remaining_clean_indices, clean_size - noisy_size)])
            
        # we align the clean and noisy data for creating transition
        # matrices, or for building cleaning model
        assert np.array_equal(clean_indices[:min(clean_size, noisy_size)], noisy_indices[:min(clean_size, noisy_size)])
        self.train_clean = np.asarray(self.train_clean_full)[clean_indices,]
        self.train_noisy = np.asarray(self.train_noisy_full)[noisy_indices,]

        # prepare clean data dataloader
        c_dataset = CoNLLDataset(self.train_clean)
        c_loader = data.DataLoader(c_dataset, batch_size=self.batch_size, shuffle=True, num_workers=self.num_workers)

        # compute noise_matrix / channel_weights
        if self.SETTINGS["USE_IDENTITY_MATRIX"]:
            noise_matrix = np.eye(self.label_representation.get_num_labels(),
                                  self.label_representation.get_num_labels())
            noise_matrix = noise_matrix * (1.0 - self.label_representation.get_num_labels() * 1e-8) + 1e-8
        else:
            s = min(clean_size, noisy_size)
            noise_matrix = self.compute_noise_matrix(self.train_clean[:s], self.train_noisy[:s])
            # if clean_size < noisy_size:
            #     noise_matrix = self.compute_noise_matrix(self.train_clean, self.train_noisy[:clean_size])
            # else:
            #     noise_matrix = self.compute_noise_matrix(self.train_clean[:noisy_size], self.train_noisy)

        # we assume that a small epsilon (1e-8) has already been added
        # e.g. inside compute_noise_matrix, so we can savely apply log
        # here
        channel_weights = np.log(noise_matrix)

        return c_loader, channel_weights


    def compute_noise_matrix(self, clean_instances, noisy_instances):
        """
        Computes a noise or confusion matrix between clean and noisy labels.

        Args:
            clean_instances: list of clean instances (GT contained in the instances)
            noisy_instances: list of noisy instances (automated labels)

        Returns:
            A noise matrix of size num_labels x num_labels. Each row represents
            p(y_noisy| y_clean=i) for a specific clean label i
            (Formula 4 in the paper, without the log)
        """

        clean_ys = np.asarray([instance.label_emb for instance in clean_instances])
        noisy_ys = np.asarray([instance.label_emb for instance in noisy_instances])

        # TODO: move this test to the data loading code: assert num_labels == len(clean_ys[0]), f'Expected {num_labels} labels, but got: {len(clean_ys[0])}'
        assert len(clean_ys) == len(noisy_ys)
        assert len(clean_ys[0]) == len(noisy_ys[0])

        num_labels = len(clean_ys[0])
        noise_matrix = np.ones((num_labels, num_labels)) * 1e-8

        for clean_y, noisy_y in zip(clean_ys, noisy_ys):
            clean_y_idx = np.argmax(clean_y)
            noisy_y_idx = np.argmax(noisy_y)
            noise_matrix[clean_y_idx, noisy_y_idx] += 1

        for row in noise_matrix:
            row_sum = np.sum(row)
            if row_sum != 0:
                row /= row_sum
            # DEBUG
            print(row, np.sum(row))
                
        # DEBUG
        print("Noise Matrix:")
        max_label_len = max([len(label) for label in self.label_representation.label_idx_to_label_name_map.values()])
        fmt_str = "{:" + str(max_label_len) + "}"

        print(" " * max_label_len, end='|')
        for i in range(len(noise_matrix)):
            print("{:7}".format(self.label_representation.label_idx_to_label_name_map[i]), end="|")
        print()
        print("-" * max_label_len, end='+')
        for _,label in sorted(self.label_representation.label_idx_to_label_name_map.items(), key=lambda x: x[0]):
            print("-" * max(4, len(self.label_representation.label_idx_to_label_name_map[i])), end="+")
        print()
        for i, row in enumerate(noise_matrix):
            print(fmt_str.format(self.label_representation.label_idx_to_label_name_map[i]), end="|")
            for cell in row:
                print("{:.04}".format(cell), end="|")
            print()

        return noise_matrix

    
    def create_BaseModel(self, embedding_vector_size, hidden_size, dense1_out_size, dense_layer1_activation, num_labels):
        feature_extractor = Feature_Extractor(embedding_vector_size, hidden_size)
        base_model = BaseModel(feature_extractor, hidden_size, dense1_out_size, dense_layer1_activation, num_labels)
        return base_model, feature_extractor


    def evaluate(self, model, epoch):
        eval_dev = self.simple_evaluation(model, self.dev, self.dev_dataloader, self.device,
                                          self.label_representation, self.SETTINGS)
        # eval_test = self.simple_evaluation(model, self.test, self.test_dataloader, self.device,
        #                                    self.label_representation, self.SETTINGS)
        # logging.info(f'Epoch {epoch + 1}\tCurrent F1 for DEV: {eval_dev}\tTEST: {eval_test}')
        logging.info("Epoch {}\tCurrent F1 for DEV: {}".format(epoch + 1, eval_dev))

        if eval_dev > self.best_dev:
            self.best_dev = eval_dev
            best_epoch = epoch
        #     test_evaluation = eval_test
        #     # TODO: improve this duplicated code
        #     self.long_test_evaluation = long_evaluation(model, self.test, self.test_dataloader, self.device,
        #                                                  self.label_representation, self.SETTINGS)

    def simple_evaluation(self, model, data, dataloader, device, label_representation, SETTINGS):
        evaluation_output = self.long_evaluation(model, data, dataloader, device, label_representation, SETTINGS)
        return Evaluation.extract_f_score(evaluation_output)
        

    def long_evaluation(self, model, data, dataloader, device, label_representation, SETTINGS):
        predictions, words = self.test_epoch(model, dataloader, device)
        predictions = label_representation.predictions_to_labels(predictions)

        # if predictions are in IO format, convert to BIO used for evaluation when working on test set
        if SETTINGS["LABEL_FORMAT"] == "io":
            predictions = LabelRepresentation.convert_io_to_bio_labels(predictions)

        evaluation = Evaluation(separator=SETTINGS["DATA_SEPARATOR"])
        connl_evaluation_string = evaluation.create_connl_evaluation_format(data, words, predictions)
        return evaluation.evaluate_evaluation_string(connl_evaluation_string)

    
    def test_epoch(self, model, dataloader, device):
        """ test a single epoch for given X and Y data.

        Args:
            model: Pytorch model used for training
        """
        model.eval()
        predictions = []
        words = []
        for xs, labels, word_batch in dataloader:
            words.extend(word_batch)
            xs = xs.to(device)
            y_pred = model(xs)
            predictions += torch.argmax(y_pred, dim=1).cpu().tolist()

        return predictions, words
    
