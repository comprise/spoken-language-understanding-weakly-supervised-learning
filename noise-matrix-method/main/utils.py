# Copyright 2020 Saarland University, Spoken Language Systems LSV
# Authors: Lukas Lange, Michael A. Hedderich, Dietrich Klakow, Thomas Kleinbauer
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
# THIS CODE IS PROVIDED *AS IS*, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
# WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
# MERCHANTABLITY OR NON-INFRINGEMENT.
#
# See the Apache 2 License for the specific language governing permissions and
# limitations under the License.

from ner_datacode import DataCreation, WordEmbedding, LabelRepresentation, Evaluation, Instance
import pickle
import os
import torch
import numpy as np

def load_and_preprocess_data(path_to_data, label_representation, remove_label_prefix, input_separator, word_embedding,
                             context_length):
    # load dataset
    data_creation = DataCreation(input_separator=input_separator)
    instances = data_creation.load_connl_dataset(path_to_data, context_length, remove_label_prefix)

    # embed words in vector representation
    word_embedding.embed_instances(instances)
    x = word_embedding.instances_to_vectors(instances)

    # convert BIO/IO labels to one hot vectors
    label_representation.embed_instances(instances)
    y = label_representation.instances_to_vectors(instances)

    return instances, x, y


def load_processed_data(path_to_data):
    with open(path_to_data, "rb") as input_file:
        data_dict = pickle.load(input_file)

    return data_dict['instances'], data_dict['remove_label_prefix'], \
           data_dict['embedding_dim']

def get_representations(Settings):
    # LabelRepresentation, either BIO or IO (for testing always BIO)
    label_representation = LabelRepresentation()
    if Settings["LABEL_FORMAT"] == "bio":
        label_representation.use_connl_bio_labels()
        test_label_representation = label_representation
    elif Settings["LABEL_FORMAT"] == "io":
        label_representation.use_connl_io_labels()
        test_label_representation = LabelRepresentation()
        test_label_representation.use_connl_bio_labels()
    else:
        raise ValueError('LABEL_FORMAT not defined in Settings')

    return label_representation, test_label_representation




def load_word_embedding(Settings):
    # Loading of fastText word embeddings
    word_embedding = WordEmbedding()
    word_embedding.load_fasttext(Settings["WORD_EMBEDDING"])
    label_representation, test_label_representation = get_representations(Settings)

    return word_embedding, label_representation, test_label_representation



def remove_label_prefix(label):
    """ CoNLL2003 distinguishes between I- and B- labels,
        e.g. I-LOC and B-LOC. Drop this distinction to
        reduce the number of labels/increase the number
        of instances per label.
    """
    if label.startswith("I-") or label.startswith("B-"):
        return label[2:]
    else:
        return label

    
def load_tsv_dataset(path, context_length, remove_label_prefix_flag=False):
    # add pre-context
    tokensAndLabels = [('<none>', None)] * context_length
        
    with open(path, mode="r", encoding="utf-8") as input_file:
        for line in input_file:
            line = line.strip()

            # sentence boundaries
            if not line:
                if tokensAndLabels[-1][-1] is not None:
                    tokensAndLabels += [('<none>', None)] * context_length
                continue
            
            # skip begin of document indicator (used in some files)
            if line.startswith("-DOCSTART-"):
                continue

            # extract token and label
            split = line.split()
            token = split[0]
            label = remove_label_prefix(split[-1]) if remove_label_prefix_flag else split[-1]

            # collect
            tokensAndLabels.append((token, label))


    # add post-context
    if tokensAndLabels[-1][-1] is not None:
        tokensAndLabels += [('<none>', None)] * context_length

    # create instances
    instances = []
    for i in range(context_length, len(tokensAndLabels) - context_length):
        token, label = tokensAndLabels[i]

        if label is not None:
            left_context = list(zip(*tokensAndLabels[i - context_length:i]))[0]
            right_context = list(zip(*tokensAndLabels[i+1:i + context_length + 1]))[0]
            
            instances.append(Instance(token, label, left_context, right_context))

    return instances

def create_pickle_data(path_to_data, label_representation, remove_label_prefix, input_separator, word_embedding,
                             context_length, save_root, save_name):
    # load dataset
    # data_creation = DataCreation(input_separator=input_separator)
    # instances = data_creation.load_connl_dataset(path_to_data, context_length, remove_label_prefix)
    import sys
    sys.path.append('.')
    instances = load_tsv_dataset(path_to_data, context_length, remove_label_prefix)

    # embed words in vector representation
    word_embedding.get_emb_for_instances(instances)

    # convert BIO/IO labels to one hot vectors
    label_representation.get_lbemb_for_instances(instances)

    # # -------------- DEBUG ---------------
    # print("--- !!! REMOVING ALL LABEL EMBEDDINGS !!! ---")
    # for instance in instances:
    #     instance.label_emb = np.zeros(len(instance.label_emb))
    # # -------------- DEBUG ---------------


    data_dict = {'instances':instances, 'remove_label_prefix': remove_label_prefix,
                 'embedding_dim': word_embedding.embedding_vector_size}
    file_path = os.path.join(save_root, save_name)

    with open(file_path, 'wb') as handle:
        #pickle.dump(data_dict, handle, protocol=pickle.HIGHEST_PROTOCOL)
        pickle.dump(data_dict, handle, protocol=4)

    print('pickle data: {} created'.format(save_name))
    return


def create_subset(instances, cs, size, random_state, sequential):
    """ Creates a subset of the data.

    Args:
        instances: list of instances
        size: Size of the subset. Samples at least 1 item if size < 1
        random_state: The subset is randomly sampled, instance of np.random_state
        sequential: If True, a random start point is picked and then a sequence of instances/words
                    is picked. If False, instances are picked randomly.

    Returns:
        subsets of corresponding xs, ys and cs

    """
    # assert len(xs) == len(ys)
    assert len(instances) >= size
    ind = _get_sample_indicies(len(instances), max(size, 1), random_state, sequential)
    instances_sub = np.array([x for i, x in enumerate(instances) if i in ind])
    cs_sub = None
    if cs is not None:
        cs_sub = np.array([y for i, y in enumerate(cs) if i in ind])
    return instances_sub, cs_sub


def _get_sample_indicies(num_items, num_samples, random_state, sequential):
    '''Returns a list of indicies that should be sampled.

    Args:
        num_items: integer value representing the pool size
        num_samples: integer value representing the number of items to be sampled
        random_state: numpy random state that should be used for random processes
        sequential: boolean value indicating whether the items should sampled sequentially or completely random

    Returns:
        A set of indices
    '''
    assert num_items >= num_samples
    numbers = list(range(num_items))
    if num_items == num_samples:
        return list(sorted(numbers))
    if sequential:
        start_number = random_state.randint(0, num_items)
        if start_number <= (num_items - num_samples):  # can generate one sequential sample
            indicies = numbers[start_number:start_number + num_samples]
        else:  # sampled would reach source list bondaries; need to generate two sequential samples
            indicies = numbers[start_number:]
            indicies.extend(numbers[:num_samples - (num_items - start_number)])
    else:
        indicies = random_state.randint(0, num_items - 1, num_samples)
    assert len(indicies) == num_samples
    return set(indicies)







def compute_noise_matrix(clean_instances, noisy_instances, label_representation):
    """
    Computes a noise or confusion matrix between clean and noisy labels.

    Args:
        clean_instances: list of clean instances (GT contained in the instances)
        noisy_instances: list of noisy instances (automated labels)

    Returns:
        A noise matrix of size num_labels x num_labels. Each row represents
        p(y_noisy| y_clean=i) for a specific clean label i
        (Formula 4 in the paper, without the log)
    """
    num_labels = label_representation.get_num_labels()
    clean_ys = np.asarray([instance.label_emb for instance in clean_instances])
    noisy_ys = np.asarray([instance.label_emb for instance in noisy_instances])


    assert num_labels == len(clean_ys[0]), "Expected {} labels, but got: {}".format(num_labels, len(clean_ys[0]))
    assert len(clean_ys) == len(noisy_ys)

    noise_matrix = np.zeros((num_labels, num_labels))

    for clean_y, noisy_y in zip(clean_ys, noisy_ys):
        clean_y_idx = np.argmax(clean_y)
        noisy_y_idx = np.argmax(noisy_y)

        noise_matrix[clean_y_idx, noisy_y_idx] += 1

    for row in noise_matrix:
        row_sum = np.sum(row)
        if row_sum != 0:
            row /= row_sum

    return noise_matrix




def train_cleaning_epoch(model, dataloader, loss_fn, optimizer, device):

    # used only for cleaning model
    model.train()

    for xs, n_labs, t_labs in dataloader:
        xs = xs.to(device)
        n_labs = n_labs.to(device)
        t_labs = t_labs.to(device)

        optimizer.zero_grad()
        y_pred = model(xs, n_labs)
        loss = loss_fn(y_pred, t_labs)
        loss.backward()
        optimizer.step()


def test_cleaning_epoch(model, dataloader, device):

    # used only for cleaning model
    model.eval()
    predictions = []
    for xs, n_labs, _ in dataloader:
        xs = xs.to(device)

        one_hot = torch.zeros(len(n_labs), 5)
        one_hot[torch.arange(len(n_labs)), n_labs] = 1
        one_hot = one_hot.to(device)

        y_pred = model(xs, one_hot)
        predictions.append(y_pred.detach().cpu().numpy())

    return np.vstack(predictions)






def train_epoch(model, dataloader, loss_fn, optimizer, device):
    """ Train a single epoch for given X and Y data.

    Args:
        model: Pytorch model used for training
    """

    model.train()

    for data in dataloader:
        xs, labels = data[0], data[1]
        xs = xs.to(device)
        labels = labels.to(device)

        optimizer.zero_grad()
        y_pred = model(xs)
        loss = loss_fn(y_pred, labels)
        loss.backward()
        optimizer.step()



def test_epoch(model, dataloader, device):
    """ test a single epoch for given X and Y data.

    Args:
        model: Pytorch model used for training
    """
    model.eval()
    predictions = []
    words = []
    for xs, labels, word_batch in dataloader:
        words.extend(word_batch)
        xs = xs.to(device)
        y_pred = model(xs)
        predictions += torch.argmax(y_pred, dim=1).cpu().tolist()

    return predictions, words






def simple_evaluation(model, data, dataloader, device, label_representation, SETTINGS):
    evaluation_output = long_evaluation(model, data, dataloader, device, label_representation, SETTINGS)
    return Evaluation.extract_f_score(evaluation_output)


def long_evaluation(model, data, dataloader, device, label_representation, SETTINGS):
    predictions, words = test_epoch(model, dataloader, device)
    predictions = label_representation.predictions_to_labels(predictions)

    # if predictions are in IO format, convert to BIO used for evaluation when working on test set
    if SETTINGS["LABEL_FORMAT"] == "io":
        predictions = LabelRepresentation.convert_io_to_bio_labels(predictions)

    evaluation = Evaluation(separator=SETTINGS["DATA_SEPARATOR"])
    connl_evaluation_string = evaluation.create_connl_evaluation_format(data, words, predictions)
    return evaluation.evaluate_evaluation_string(connl_evaluation_string)

