# Copyright 2020 Saarland University, Spoken Language Systems LSV
# Authors: Lukas Lange, Michael A. Hedderich, Dietrich Klakow, Thomas Kleinbauer
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
# THIS CODE IS PROVIDED *AS IS*, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
# WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
# MERCHANTABLITY OR NON-INFRINGEMENT.
#
# See the Apache 2 License for the specific language governing permissions and
# limitations under the License.

import numpy as np
import torch
from torch.utils import data


class CoNLLDataset(data.Dataset):
    def __init__(self, instances, onehot=False):
        self.instances = instances
        embs = np.asarray([ins.embedding for ins in instances])
        labels = np.asarray([ins.label_emb for ins in instances])
        self.words = [ins.word for ins in instances]

        # convert to torch
        self.embs = torch.from_numpy(embs).float()
        if onehot:
            self.labels = torch.from_numpy(labels).float()
        else:
            self.labels = torch.from_numpy(labels).argmax(dim=1)

    def __len__(self):
        return len(self.instances)

    def __getitem__(self, index):
        emb = self.embs[index]
        label = self.labels[index]
        word = self.words[index]
        return emb, label, word

    
# A more memory-efficient (but slower?) implementation of the above class
# class CoNLLDataset(data.Dataset):
#     def __init__(self, instances, onehot=False):
#         self.instances = instances
#         self.onehot = onehot

#     def __len__(self):
#         return len(self.instances)

#     def __getitem__(self, index):
#         ins = self.instances[index]

#         emb = torch.from_numpy(ins.embedding).float()
#         label = torch.from_numpy(ins.label_emb)
#         if self.onehot:
#             label = label.float()
#         else:
#             label = label.argmax()

#         return emb, label, ins.word
    

